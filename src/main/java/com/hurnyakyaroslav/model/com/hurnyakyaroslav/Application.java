package com.hurnyakyaroslav.model.com.hurnyakyaroslav;

import com.hurnyakyaroslav.model.beans.BeanA;
import com.hurnyakyaroslav.model.beans.BeanE;
import com.hurnyakyaroslav.model.configurations.GeneralConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        String answer = "";
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(GeneralConfig.class);
         BeanE beanE = applicationContext.getBean("beanEFromA1", BeanE.class);
        BeanA beanA = applicationContext.getBean("fromBC", BeanA.class);

        answer += beanA.toString() + " = ";
         answer += beanE.toString();
        applicationContext.close();
        System.out.println(answer);
        System.out.println("END");
    }
}
