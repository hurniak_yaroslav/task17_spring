package com.hurnyakyaroslav.model;

public interface BeanValidator {
    boolean validate();
}
