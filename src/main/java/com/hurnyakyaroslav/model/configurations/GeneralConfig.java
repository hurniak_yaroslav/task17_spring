package com.hurnyakyaroslav.model.configurations;

import com.hurnyakyaroslav.model.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("my.properties")
public class GeneralConfig {

    @Value("${beanB.name}")
    private String beanBName;
    @Value("${beanB.value}")
    private int beanBValue;
    @Value("${beanC.name}")
    private String beanCName;
    @Value("${beanC.value}")
    private int beanCValue;
    @Value("${beanD.name}")
    private String beanDName;
    @Value("${beanD.value}")
    private int beanDValue;

        @Bean(name = "fromBC")
    public BeanA getBeanAFromBC(BeanB beanB, BeanC beanC) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanC.getValue());
        return beanA;
    }

    @Bean(name = "fromBD")
    @Order(4)
    public BeanA getBeanAFromBD(BeanB beanB, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean(name = "fromCD")
    public BeanA getBeanAFromCD(BeanC beanC, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanC.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }
    //////////////////////////

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(1)
    public BeanB getBeanB() {
        return new BeanB(beanBName, beanBValue);
    }


//////////////////

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC(){
        return new BeanC(beanCName, beanCValue);
    }


    ///////////////

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(3)
    public BeanD getBeanD(){
        return new BeanD(beanDName, beanDValue);
    }
    
    ///////////////////

    @Bean("beanEFromA1")
    public BeanE getBeanEFirst(@Qualifier("fromCD") BeanA fromBC) {
        return new BeanE(fromBC.getName(), fromBC.getValue());
    }

    @Bean("beanEFromA2")
    public BeanE getBeanESecond(BeanA fromBD) {         //can use Qualifier annotation
        return new BeanE(fromBD.getName(), fromBD.getValue());
    }

    @Bean("beanEFromA3")
    public BeanE getBeanEThird(BeanA fromCD) {
        return new BeanE(fromCD.getName(), fromCD.getValue());
    }

}
