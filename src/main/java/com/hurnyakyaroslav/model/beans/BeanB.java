package com.hurnyakyaroslav.model.beans;

import com.hurnyakyaroslav.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB implements BeanValidator {

    private String name;
    private int value;
    private static Logger logger = LogManager.getLogger(BeanB.class);

    public BeanB() {
        System.out.println(value);
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }


    @Override
    public boolean validate() {
        if (name.equals("Nazar")) return false;
        else return true;
    }


    public void init() {
        logger.info("Init method for beanB: " + this.getClass().getName() + " init");
    }

    public void destroy() {
        logger.info("Destroy method for beanB: " + this.getClass().getName() + " destroy");
    }
}
