package com.hurnyakyaroslav.model.beans;

import com.hurnyakyaroslav.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    String name;
    int value;
    private static Logger logger = LogManager.getLogger(BeanE.class);

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }


    @Override
    public boolean validate() {
        if (name.equals("Nazar")) return false;
        else return true;
    }

    @PostConstruct
    public void init() {
        logger.info(this.getClass().getName() + " PostConstruct init");
    }

    @PreDestroy
    public void destroy() {
        logger.info(this.getClass().getName() + " PreDestroy");
    }

}