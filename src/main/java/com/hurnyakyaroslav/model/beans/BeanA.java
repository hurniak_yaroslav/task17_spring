package com.hurnyakyaroslav.model.beans;

import com.hurnyakyaroslav.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    String name;
    int value;
    private static Logger logger = LogManager.getLogger(BeanA.class);

    public BeanA() {

    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }


    @Override
    public boolean validate() {
        if (name.equals("Nazar")) return false;
        else return true;
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Destroy method for beanC: " + this.getClass().getName() + " destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Alternative initialization: " + this.getClass().getName());
    }
}
