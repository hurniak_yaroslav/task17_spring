package com.hurnyakyaroslav.model.beans;

import com.hurnyakyaroslav.model.BeanValidator;

public class BeanF implements BeanValidator {
    String name;
    int value;

    public BeanF() {
    }

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }


    @Override
    public boolean validate() {
        if (name.equals("Nazar")) return false;
        else return true;
    }
}