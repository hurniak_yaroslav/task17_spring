package com.hurnyakyaroslav.model.beans;

import com.hurnyakyaroslav.model.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CustomPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String name) throws BeansException {

        if (bean instanceof BeanValidator) {
            ((BeanValidator) bean).validate();
        }
        if (bean instanceof BeanA) {
            if (((BeanA) bean).getName().isEmpty() || ((BeanA) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        if (bean instanceof BeanB) {
            if (((BeanB) bean).getName().isEmpty() || ((BeanB) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        if (bean instanceof BeanC) {
            if (((BeanC) bean).getName().isEmpty() || ((BeanC) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        if (bean instanceof BeanD) {
            if (((BeanD) bean).getName().isEmpty() || ((BeanD) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        if (bean instanceof BeanE) {
            if (((BeanE) bean).getName().isEmpty() || ((BeanE) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        if (bean instanceof BeanF) {
            if (((BeanF) bean).getName().isEmpty() || ((BeanF) bean).getValue() < 0) {
                throw new MyBeansException(name);
            }
        }
        return bean;
    }
}

class MyBeansException extends BeansException {

    public MyBeansException(String msg) {
        super(msg);
    }

}
